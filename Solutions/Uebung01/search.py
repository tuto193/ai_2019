from maze import Maze
from collections import deque
import numpy as np
import matplotlib.pyplot as plt

def breadth_first_search(maze, start, goal, plot=False):

    if plot:
        # prepare the plot
        plt.ion()
        fig = maze.plot()
        fig.gca().set_title("Breadth first")
        path_plot = fig.gca().scatter([], [], c='lightcoral', zorder=2, alpha=0.8)
        visited_plot = fig.gca().scatter([], [], c='grey', zorder=1, alpha=0.8)

    # prepare the housekeeping stuff
    queue = deque([([start], start)])
    visited = set()
    max_depth = 0
    max_frontier = 0
    step_num = 0

    while queue:
        path, current = queue.popleft() # pop the leftmost value
        if current == goal:
            if plot:
                plt.ioff()
            # we are done
            return path, (visited, max_depth, max_frontier)
        elif current not in visited:
            # expand node
            path_visited = set(path)
            neighbors = maze.get_neighbors(current, path_visited)
            for neighbour in neighbors:
                p = list(path)
                p.append(neighbour)
                queue.append((p, neighbour))
            visited.add(current)

        if len(queue) > max_frontier:
                max_frontier = len(queue)
        if len(path) > max_depth:
            max_depth = len(path)

        if plot:
            input() # wait for keypress
            path_plot.remove()
            visited_plot.remove()

            p = np.array(path).T
            v = np.array(list(visited)).T

            print("Current Step:", step_num)
            print("\tPath length (depth):", len(path))
            print("\tNodes in Frontier:", [q[1] for q in queue])
            print("\tCurrent frontier size:", len(queue))
            print("\tMax Frontier size:", max_frontier)
            print("\tMax depth:", max_depth)

            path_plot = fig.gca().scatter(p[1], p[0], c='lightcoral', zorder=2, alpha=0.8)
            visited_plot = fig.gca().scatter(v[1], v[0], c='grey', zorder=1, alpha=0.8)
            fig.canvas.draw()
        step_num += 1

    return None, (visited, max_depth, max_frontier)

def depth_first_search(maze, start, goal, depth_bound=None, plot=False, fig=None):

    def get_path(coord, visited):
        path = []

        if coord is None:
            return path
        if not coord in visited:
            return path

        while not coord is None:
            path.append(coord)
            coord = visited[coord]

        return path

    if plot:
        plt.ion()
        if fig is None:
            fig = maze.plot()
            fig.gca().set_title("Depth first")
        path_plot = fig.gca().scatter([], [], c='lightcoral', zorder=2, alpha=0.8)
        visited_plot = fig.gca().scatter([], [], c='grey', zorder=1, alpha=0.8)

    # suchfront (node, parent)
    queue = deque([(start, None)])

    # visited for back traversal of path
    # key: coord, value: parent
    visited = dict()


    max_depth = 0
    max_frontier = 0
    step_num = 0

    draw_until = 120

    while queue:
        current, parent = queue.pop()
        if current in visited:
            # check if path is worse
            existing_path_len = len(get_path(current, visited))
            new_path_len = len(get_path(parent, visited)) + 1

            if existing_path_len < new_path_len:
                continue

        visited[current] = parent
        path = get_path(current, visited)

        # check if current is at goal
        if current == goal:
            return path, (visited.keys(), max_depth, max_frontier)

        elif depth_bound is not None and len(path) >= depth_bound:
            continue

        # prepare to expand
        path_visited = set(path)
        neighbors = maze.get_neighbors(current, path_visited)

        for neighbour in neighbors:
            queue.append((neighbour, current))

        step_num += 1

        if len(path) > max_depth:
            max_depth = len(path)
        if len(queue) > max_frontier:
            max_frontier = len(queue)

        if plot and step_num > draw_until:
            input() # wait for keypress
            path_plot.remove()
            visited_plot.remove()

            p = np.array(path).T
            v = np.array(list(visited.keys())).T
            print("Current Step:", step_num)
            print("\tPath length (depth):", len(path))
            print("\tNodes in Frontier:", [q[0] for q in queue])
            print("\tCurrent frontier size:", len(queue))
            print("\tMax Frontier size:", max_frontier)
            print("\tMax depth:", max_depth)
            print("\tCurrent path:", path)
            # print("\tFrontier:", queue)
            path_plot = fig.gca().scatter(p[1], p[0], c='lightcoral', zorder=2, alpha=0.8)
            visited_plot = fig.gca().scatter(v[1], v[0], c='grey', zorder=1, alpha=0.8)
            fig.canvas.draw()

    if plot:
        path_plot.remove()
        visited_plot.remove()

    return None, (visited.keys(), max_depth, max_frontier)

def iterative_deepening(maze, start, goal, plot=False):
    fig = None
    if plot:
        plt.ion()
        fig = maze.plot()
        fig.gca().set_title("Iterative deepening")

    path = None
    stats = [set(), 0,0]
    bound = 1

    while path is None:
        # if bound > 121:
        #     break
        if plot:
            print("-------------------------")
            print("Iteration:", bound)
        path, s = depth_first_search(maze, start, goal, plot=plot, fig=fig, depth_bound=bound)
        # keep track of the stats
        if path is None:
            stats[0] = stats[0].union(s[0])
            stats[1] = max(stats[1], s[1])
            stats[2] = max(stats[2], s[2])
        else:
            stats[0] = stats[0].union(s[0])
            stats[1] = max(stats[1], s[1])
            stats[2] = max(stats[2], s[2])
        bound += 1

    return path, tuple(stats)

def naive_search(maze, start, goal):
    from random import choice
    pos = start
    path = [pos]
    while pos != goal:
        # always take a random action
        neighbors = maze.get_neighbors(pos)
        pos = choice(neighbors)
        path.append(pos)
    path = np.array(path).T
    return path, ()





if __name__ == "__main__":
    plot = False
    m = Maze.load_from_file("labyrinth_50x50.maze")
    #m = Maze((7,7))
    # path, stats = naive_search(m, m.start, m.end)
    # print("naive search length", len(path[0]))
    # fig = m.plot()
    # fig.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)

    path, stats = breadth_first_search(m, m.start, m.end, plot=plot)
    path = np.array(path).T
    visited = np.array(list(stats[0])).T
    print("Breadth first: \nnum expanded: {}\nmaximum depth: {}\nmaximum frontier size: {}\npath length: {}".format(len(visited[0]), stats[1], stats[2], len(path[0])))
    fig_bfs = m.plot()
    fig_bfs.gca().set_title("Breadth first")
    path_plot = fig_bfs.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)
    visited_plot = fig_bfs.gca().scatter(visited[1], visited[0], c='grey', zorder=1, alpha=0.8)

    path, stats = depth_first_search(m, m.start, m.end, plot=plot)
    path = np.array(path).T
    visited = np.array(list(stats[0])).T
    print("Depth first: \nnum expanded: {}\nmaximum depth: {}\nmaximum frontier size: {}\npath length: {}".format(len(visited[0]), stats[1], stats[2], len(path[0])))
    fig_dfs = m.plot()
    fig_dfs.gca().set_title("Depth first")
    fig_dfs.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)
    fig_dfs.gca().scatter(visited[1], visited[0], c='grey', zorder=1, alpha=0.8)

    # path, stats = depth_search(m, m.start, m.end)
    path, stats = iterative_deepening(m, m.start, m.end, plot=plot)

    if path is None:
        print('path is none')
        exit()

    path = np.array(path).T
    visited = np.array(list(stats[0])).T
    print("Iterative deepening: \nnum expanded: {}\nmaximum depth: {}\nmaximum frontier size: {}\npath length: {}".format(len(visited[0]), stats[1], stats[2], len(path[0])))
    fig_dfs = m.plot()
    fig_dfs.gca().set_title("Iterative deepening")
    fig_dfs.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)
    fig_dfs.gca().scatter(visited[1], visited[0], c='grey', zorder=1, alpha=0.8)

    plt.show()