import numpy as np

def p(state):
    """
    Returns the correct element depending on 'state'
    :param state: the game state
    :return: 'X' if state is greater than 0, 'O' if state is less than 0 and ' ' otherwise
    """
    if   state > 0:
        return "X"
    elif state < 0:
        return "O"
    else:
        return " "

class ConnectFour(object):

    def __init__(self):
        """
        Constructor
        """
        self.state = 1
        self.board = np.zeros((6,7), dtype=np.int8)

    def __str__(self):
        """
        Returns the game representation as string
        :return:
        """
        rows = self.board.shape[0]
        cols = self.board.shape[1]

        res = "\n   "
        for col in range(cols):
            res += "  " + str(col) + " "
        res += " \n"

        res += "   " + cols * "----" + "-\n"

        for row in range(rows):
            res += " " + str(row) + " "
            for col in range(cols):
                res += "| " + p(self.board[row, col]) + " "
            res += "|\n"
            res += "   " + cols * "----" + "-\n"

        res += "\n   "
        if self.state == 0:
            res += "Draw"
        elif abs(self.state) == 1:
            res += "Player " + p(self.state) + " to move"
        else:
            res += "Player " + p(self.state) + " has won"
        res += "\n"

        return res

    def move(self, col) -> bool:
        """
        Try to insert a stone in the given column. On success the board state is updated.
        :param col: the column to insert the stone
        :return: True if the move was successful, False otherwise
        """
        if self.is_over():
            return False

        rows = self.board.shape[0]

        for row in reversed(range(rows)):
            if self.board[row, col] == 0:
                self.board[row, col] = self.state
                self.__update_state(row, col)
                return True

        return False

    def __update_state(self, row, col) -> None:
        """
        Update the game state. Internal method, not for external use.
        :param row: the row to update
        :param col: the column to update
        """
        rows = self.board.shape[0]
        cols = self.board.shape[1]

        def f(drow, dcol):
            return     0 <= row + drow < rows \
                   and 0 <= col + dcol < cols \
                   and bool(self.board[row, col] == self.board[row + drow, col + dcol])

        def g(drow, dcol):
            len = 0
            for i in range(1, 4):
                if f(i * drow, i * dcol):
                    len += 1
                else:
                    break
            return len

        def h(drow, dcol):
            return g(-drow, -dcol) + g(drow, dcol) >= 3

        if np.all(self.board[0, :] != 0):
            self.state = 0
        if h(1, 0) or h(0, 1) or h(1, 1) or h(-1, 1):
            self.state *= 2
        else:
            self.state *= -1

    def is_over(self) -> bool:
        """
        Check if the game is over
        :return: True if over, False otherwise
        """
        return abs(self.state) != 1


if __name__ == "__main__":
    c4 = ConnectFour()
    while not c4.is_over():
        print(c4)
        try:
            line = input("   What's your move? ")
        except (EOFError, KeyboardInterrupt):
            print()
            exit()
        try:
            move = int(line)
        except ValueError:
            print("   Invalid move")
            continue
        try:
            c4.move(move)
        except IndexError:
            print("   Invalid move")
            continue
    print(c4)
