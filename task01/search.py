from maze import Maze
import numpy as np
import matplotlib.pyplot as plt

def naive_search(maze, start, goal):
    from random import choice
    pos = start
    path = [pos]
    while pos != goal:
        # always take a random action
        neighbors = maze.get_neighbors(pos)
        pos = choice(neighbors)
        path.append(pos)
    path = np.array(path).T
    return path, ()

def dfs( maze, start, goal ):
    pass


if __name__ == "__main__":
    m = Maze.load_from_file("labyrinth_100x100.maze")
    path, stats = naive_search(m, m.start, m.end)
    print("naive search path length", len(path[0]))
    fig = m.plot()
    fig.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)
    plt.show()