import matplotlib
import networkx as nx
import osmnx as ox
import matplotlib.pyplot as plt

# configure osmnx to cache the queries and log what it does (lots of text!)
ox.config(use_cache=True, 
          log_console=True)

# use data from file?
load_from_file = False

# positions of some locations
wachsbleiche = (52.2852171, 8.0365810704887)
goal = (52.272235, 8.060066)

# use the geocoding-service instead (WARNING: sometimes this doesn't work)
#wachsbleiche = ox.utils.geocode("Wachsbleiche 27, Osnabrück, Deutschland")

if load_from_file:
    try:
        G = ox.load_graphml('osnabrueck.graphml', folder='data')
    except FileNotFoundError as e:
        raise FileNotFoundError("Graphml file 'osnabrueck.graphml' not found")
else:
    # fetch it from the osm servers
    G = ox.graph_from_point(wachsbleiche, distance=20000, network_type='drive', simplify=False)

# get the closest nodes in the graph for all positions as start and end
wachsbleiche_node = ox.get_nearest_node(G, wachsbleiche)
goal = (52.272235, 8.060066)
goal_node = ox.get_nearest_node(G, goal)

def node_distance(graph, node1, node2):
    """
    Return the distance between the two graph nodes in meters.
    :param graph: networkx.Graph 
        The graph to look up the nodes
    :param node1: int
        The node id of the first node
    :param node2: int
        The node id of the second node
    :return: float
        Distance between the two node in meters
    """
    return ox.great_circle_vec(graph.nodes[node1]['x'], 
                                 graph.nodes[node1]['y'],
                                 graph.nodes[node2]['x'],
                                 graph.nodes[node1]['y'])

# print the node data for the start
print("start node data:", G.nodes[wachsbleiche_node])

# print the distance between start and goal
print("distance between start and goal:", node_distance(G, wachsbleiche_node, goal_node))


# print the edge data for all edges at the starting node
print("edges around the start:")
for s, e, data in G.edges(wachsbleiche_node, data=True):
    print("\t", data)

# get a route (dijkstra; not the best!!!)
route = nx.shortest_path(G, wachsbleiche_node, goal_node)

if route:
    fig, ax = ox.plot_graph_route(G, route, show=False, close=False, axis_off=False, node_color='#66ccff')
else:
    # plot just the graph
    fig, ax = ox.plot_graph(G, show=False, close=False, axis_off=False, annotate=False)

# plot start and end positions of the route (reverse, because the coordinates are returned in the wrong order for matplotlib!)
ax.scatter(*reversed(wachsbleiche), c='r', s=15, zorder=3)
ax.scatter(*reversed(goal), c='g', s=15, zorder=3)

# print route information
print("route nodes:")
for i in range(len(route)-1):
    print("\t", G.get_edge_data(route[i], route[i+1]))

plt.show()


