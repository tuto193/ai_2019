/* WISSENSBASIS der Griechische Goetter und Giganten */ 

/* FAKTEN */ 
weiblich(asteria).
weiblich(demeter).
weiblich(gaia).
weiblich(artemis). 
weiblich(athene). 
weiblich(eleithyia). 
weiblich(hebe).
weiblich(hera).
weiblich(leto). 
weiblich(hecate). 
weiblich(hestia).
weiblich(metis).
weiblich(mnemosyne). 
weiblich(persephone).
weiblich(phoibe).
weiblich(rhea). 
weiblich(selene). 
weiblich(themis).
weiblich(tethys).
weiblich(theia). 

maennlich(apollon). 
maennlich(ares). 
maennlich(chaos).
maennlich(chronos).
maennlich(eos). 
maennlich(hades). 
maennlich(helios). 
maennlich(hyperion).
maennlich(koios).
maennlich(iapetos).
maennlich(okeanos).
maennlich(perses). 
maennlich(poseidon).
maennlich(uranos).
maennlich(zeus). 

/*  Liste der Elternteile (Mutter oder Vater) und ihrer Kinder: elternteil(Elternteil,Kind) */ 
elternteil(chaos,gaia). 
elternteil(chronos,poseidon). 
elternteil(hyperion,eos). 
elternteil(hyperion,helios). 
elternteil(hyperion,selene). 
elternteil(koios,asteria). 
elternteil(koios,leto). 
elternteil(chronos,demeter). 
elternteil(chronos,hades). 
elternteil(chronos,hera). 
elternteil(chronos,hestia). 
elternteil(chronos,zeus). 
elternteil(okeanos,fluesse). 
elternteil(okeanos,metis). 
elternteil(okeanos,okeaniden). 
elternteil(perses,hekate). 
elternteil(uranos,hecatoncheiren). 
elternteil(uranos,hyperion). 
elternteil(uranos,iapetos). 
elternteil(uranos,koios). 
elternteil(uranos,krios). 
elternteil(uranos,chronos). 
elternteil(uranos,kyklopen). 
elternteil(uranos,mnemosyne). 
elternteil(uranos,okeanos). 
elternteil(uranos,phoibe). 
elternteil(uranos,rhea). 
elternteil(uranos,tethys). 
elternteil(uranos,theia). 
elternteil(uranos,themis). 
elternteil(zeus,apollon). 
elternteil(zeus,ares). 
elternteil(zeus,artemis). 
elternteil(zeus,athene). 
elternteil(zeus,eleithyia). 
elternteil(zeus,hebe). 
elternteil(zeus,musen). 
elternteil(zeus,persephone). 
elternteil(asteria,hecate). 
elternteil(demeter,persephone). 
elternteil(gaia,hecatoncheiren). 
elternteil(gaia,hyperion). 
elternteil(gaia,iapetos). 
elternteil(gaia,koios). 
elternteil(gaia,krios). 
elternteil(gaia,chronos). 
elternteil(gaia,kyklopen). 
elternteil(gaia,mnemosyne). 
elternteil(gaia,okeanos). 
elternteil(gaia,phoibe). 
elternteil(gaia,rhea). 
elternteil(gaia,tethys). 
elternteil(gaia,theia). 
elternteil(gaia,themis). 
elternteil(gaia,uranos).  
elternteil(hera,ares). 
elternteil(hera,eleithyia). 
elternteil(hera,hebe). 
elternteil(hera,hephaistos). 
elternteil(leto,apollon). 
elternteil(leto,artemis). 
elternteil(metis,athene). 
elternteil(mnemosyne,musen). 
elternteil(phoibe,asteria). 
elternteil(phoibe,leto). 
elternteil(rhea,demeter). 
elternteil(rhea,hades). 
elternteil(rhea,hera). 
elternteil(rhea,hestia). 
elternteil(rhea,poseidon). 
elternteil(rhea,zeus). 
elternteil(tethys,fluesse). 
elternteil(tethys,metis). 
elternteil(tethys,okeaniden). 
elternteil(theia,eos). 
elternteil(theia,helios). 
elternteil(theia,selene). 

/* Regeln */
vater_von(V, K) :-
    maennlich(V),
    elternteil(V, K).
mutter_von(M, K) :-
    weiblich(M),
    elternteil(M, K).
eltern_von(V, M, K) :-
    #elternteil(V, K),
    #elternteil(M, K),
    #M /= V.
    (vater_von(V, K),
    mutter_von(M, K)).
sohn_von(S, E) :-
    maennlich(S),
    elternteil(E, S).
tochter_von(T, E) :-
    weiblich(T),
    elternteil(E, T).
geschwister(K1, K2) :-
    elternteil(E, K1),
    elternteil(E, K2),
    K1 \= K2.
bruder_von(B, K) :-
    maennlich(B),
    geschwister(B, K).
schwester_von(S, K) :-
    weiblich(S),
    geschwister(S, K).
onkel_von(O, K) :-
    elternteil(E, K),
    bruder_von(O, E).
tante_von(T, K) :-
    elternteil(E, K),
    schwester_von(T, E).
grossvater_von(G, K) :-
    elternteil(E, K),
    vater_von(G, E).
enkel_von(K, G) :-
    elternteil(G, E),
    elternteil(E, K).
vorfahr(V, N) :-
    elternteil(V, N).
vorfahr(V, N) :-
    elternteil(E, N),
    vorfahr(V, E).
nachfahr(N, V) :-
    vorfahr(V, N).
