/* Fakten */
wochentag('09.12.2019',montag).
wochentag('10.12.2019',dienstag).
wochentag('11.11.2019',mittwoch).
wochentag('12.11.2019',donnerstag).
wochentag('13.11.2019',freitag).
wochentag('14.11.2019',samstag).
wochentag('15.11.2019',sonntag).

gericht(lachs,fisch).
gericht(scholle,fisch).
gericht(kabeljau,fisch).
gericht(schwein,fleisch).
gericht(rind,fleisch).
gericht(wild,fleisch).
gericht(kartoffelknoedel,knoedel).
gericht(semmelknoedel,knoedel).
gericht(spinatknoedel,knoedel).
gericht(tomatensuppe,tagessuppe).
gericht(nudelsuppe,tagessuppe).
gericht(kartoffelsuppe,tagessuppe).
gericht(kartoffelsalat,salat).
gericht(gemischter_salat,salat).
gericht(krautsalat,salat).
gericht(nuernberger,wurst).
gericht(krakauer,wurst).
gericht(wiener,wurst).

gibt_in_Mensa('09.12.2019',tomatensuppe).
gibt_in_Mensa('09.12.2019',pommes).
gibt_in_Mensa('10.12.2019',wild).
gibt_in_Mensa('10.12.2019',semmelknoedel).
gibt_in_Mensa('10.12.2019',gemischter_salat).
gibt_in_Mensa('11.12.2019',kartoffelsuppe).
gibt_in_Mensa('11.12.2019',pommes).
gibt_in_Mensa('12.12.2019',nudelsuppe).
gibt_in_Mensa('12.12.2019',krakauer).
gibt_in_Mensa('12.12.2019',kartoffelsalat).
gibt_in_Mensa('13.12.2019',lachs).
gibt_in_Mensa('13.12.2019',krautsalat).

student(stefan).
student(peter).
student(jochen).
student(dirk).
student(torben).

geht_in_Mensa(stefan).
geht_in_Mensa(peter).
geht_in_Mensa(torben).

/* Regeln */
isst(stefan,X,Y) :-
    gibt_in_Mensa(X,Y),
    gericht(Y,wurst).
