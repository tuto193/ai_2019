import numpy as np

from connect_four import ConnectFour
from math import inf



class Solver:
    def __init__(self, player):
        if player == 'X':
            self.player = 1
        elif player == 'O':
            self.player = -1
        else:
            raise ValueError

    def take_move(self, board:ConnectFour().board ) -> int:
        # TODO: do something with the current board
        #implement minimax (test depth of )
        return np.random.randint(7)

    def minimax( self, board:ConnectFour().board, node:int, depth:int=4, maximizingPlayer:bool=True ) -> int:
        """
        Run Minimax search on a certain node at the c4 board
        function minimax(node, depth, maximizingPlayer) is
            if depth == 0 or node is a terminal node then
                return the heuristic value of node
            if maximizingPlayer then
                value := −∞
                for each child of node do
                    value := max(value, minimax(child, depth − 1, FALSE))
                return value
            else (* minimizing player *)
                value := +∞
                for each child of node do
                    value := min(value, minimax(child, depth − 1, TRUE))
                return value
        """
        value:int = 0
        if depth == 0 or board.is_over():
            # we can't go deeper or we should just calculate here what we have
            # the board is 6 high and 7 wide
            p1:str='X' if self.player == 1 else 'O'
            p2:str='O' if p1 == 'X' else 'X'
            currentPlayer:str=p1 if maximizingPlayer else p2
            multiplier:int= 1 if maximizingPlayer else -1
            lb:int = node
            rb:int = 6 - lb
            row:int = find_row( board, node)
            alternated:bool = False
            connecting:bool = False
            total_connected_l:int = 0
            total_connected_r:int = 0
            added_value:int = 0
            i:int = 0
            # check horizontally -
            if(lb > 0):
                lb = lb - 1
                checking:str = board[row,lb]
                connecting = True if (checking == currentPlayer or checking == ' ' ) else False
                added_value = 1 if checking == currentPlayer else 0
                i = 2
                lb -= 1
                while( lb >= 1 and i >= 1 and not alternated ):
                    checking2:str = board[row, lb]
                    if( checking ==  checking2 ):
                        if( checking != ' ' ):
                            if( connecting ):
                                total_connected_l += 1
                                added_value += 1
                        else:
                            added_value += 1
                        elif( connecting ):


                    lb -= 1
                    i -= 1

            # check vertically |
            #check diagonally upwards  /
            #check diagonally downwards \


        # otherwise we go business as usual
        if maximizingPlayer:
            value = -inf
        return 0


def find_row( b:ConnectFour().board, col:int ) -> int:
    """
    Finds the correct height r
    returns: the row on which the column has placed (lowest == 1)
    """
    r:int = b.shape[1] #initialise at maximum height
    for row in reversed(range(b.shape[0]) ):
        if(b[row, col] == 0):
            r -= 1
    return r



if __name__ == "__main__":
    c4:ConnectFour = ConnectFour()
    com:Solver = Solver('O')
    while not c4.is_over():
        print(c4)
        if c4.state == com.player:
            move = com.take_move(c4.board)
        else:
            try:
                line = input("   What's your move? ")
            except (EOFError, KeyboardInterrupt):
                print()
                exit()
            try:
                move = int(line)
            except ValueError:
                print("   Invalid move")
                continue
        try:
            c4.move(move)
        except IndexError:
            print("   Invalid move")
            continue
    print(c4)
