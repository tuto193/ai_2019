# jps

from maze import Maze
from collections import deque
import numpy as np
import matplotlib.pyplot as plt

def manhattandistanz(cord, goal):
    return abs(cord[0] - goal[0]) + abs(cord[1] - goal[1])

def bestensuche(maze, start, goal):
    suchfront = list([(manhattandistanz(start, goal),[start], start)])
    visited = set()

    while len(suchfront) > 0:
        distanz, path, currentNode = suchfront.pop(0)

        if currentNode == goal:
            return path, visited
        elif currentNode not in visited:
            neighbours = maze.get_neighbors(currentNode, visited)

            for neighbour in neighbours:
                p = list(path)
                p.append(neighbour)
                suchfront.append((manhattandistanz(neighbour, goal),p, neighbour))
            visited.add(currentNode)
            suchfront.sort()
    return None, None

def astern(maze, start, goal):
    suchfront = list([(0, 0, [start], start)])
    visited = set()

    while len(suchfront) > 0:
        currentf, currentg, path, currentNode = suchfront.pop(0)

        if currentNode == goal:
            return path, visited
        elif currentNode not in visited:
            neighbours = maze.get_neighbors(currentNode, visited)

            for neighbour in neighbours:
                p = list(path)
                p.append(neighbour)
                suchfront.append((manhattandistanz(neighbour, goal) + currentg, currentg  + 1, p, neighbour))
            visited.add(currentNode)
            suchfront.sort()
    return None, None


def breitensuche(maze, start, goal):
    suchfront = deque([([start], start)])
    visited = set()

    while suchfront:
        path, current = suchfront.popleft()
        if current == goal:
            return path, visited
        elif current not in visited:
            neighbors = maze.get_neighbors(current, visited)
            for neighbour in neighbors:
                p = list(path)
                p.append(neighbour)
                suchfront.append((p, neighbour))
            visited.add(current)

    return None, None

if __name__ == "__main__":
    m = Maze.load_from_file("labyrinth_100x100.maze")

    path, visited = breitensuche(m, m.start, m.end)
    print("Breitensuche: " + str(len(path)))
    path = np.array(path).T
    visited = np.array(list(visited)).T
    fig = m.plot()
    fig.gca().set_title("Breitensuche")
    path_plot = fig.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)
    path_plot = fig.gca().scatter(visited[1], visited[0], c='grey', zorder=1, alpha=0.8)

    path, visited = bestensuche(m, m.start, m.end)
    print("Bestensuche: " + str(len(path)))
    path = np.array(path).T
    visited = np.array(list(visited)).T
    fig = m.plot()
    fig.gca().set_title("Bestensuche")
    path_plot = fig.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)
    path_plot = fig.gca().scatter(visited[1], visited[0], c='grey', zorder=1, alpha=0.8)


    path, visited = astern(m, m.start, m.end)
    print("A*: " + str(len(path)))
    path = np.array(path).T
    visited = np.array(list(visited)).T
    fig = m.plot()
    fig.gca().set_title("A*")
    path_plot = fig.gca().scatter(path[1], path[0], c='lightcoral', zorder=2, alpha=0.8)
    path_plot = fig.gca().scatter(visited[1], visited[0], c='grey', zorder=1, alpha=0.8)

    plt.show()
