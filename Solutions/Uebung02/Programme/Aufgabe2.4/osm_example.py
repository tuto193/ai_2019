import matplotlib
import networkx as nx
import osmnx as ox
import matplotlib.pyplot as plt
import time


def route_length(graph, route):
    length = 0
    for i in range(1, len(route)):
        ed = graph.get_edge_data(route[i-1], route[i])
        try:
            l = ed[0]['length']
        except KeyError:
            l = ed['length']
        length += l

    return length

def a_star(graph, start, end):
    open_list = []
    # closed_list = set()

    closed_dict = dict()

    open_list.append((start, 0, node_distance(graph, start, end)))
    graph.nodes[start]['parent'] = None
    #print("starting at", start)
    while open_list:
        current_node, node_cost, total_cost = open_list.pop(0)
        # closed_list.add(current_node)
        closed_dict[current_node] = (node_cost, total_cost)
        #print("current node", current_node, node_cost)
        if current_node == end:
            path = []
            current = current_node
            while current is not None:
                path.append(current)
                current = graph.nodes[current]['parent']
            return path[::-1] # reversed path

        # compute all neighbors

        for nbr in graph[current_node]:
            try:
                node_to_neighbor_cost = min([nb['length'] for k, nb in graph[current_node][nbr].items()])
            except:
                node_to_neighbor_cost = graph[current_node][nbr]['length']

            g = node_cost + node_to_neighbor_cost
            # heuristic cost
            h = node_distance(graph, nbr, end)
            # total cost
            f = g+h

            # skip neighbor that was already processed
            # and thats total_cost is smaller than the current neighbors cost
            if nbr in closed_dict and f >= closed_dict[nbr][1]:
                continue

            # search for neighbors to be in open_list
            # continue if neighbor was found in open_list that has a better cost
            # update neighbor in open list if current has better cost
            dup_nodes = [(i,x) for (i, x) in enumerate(open_list) if nbr == x[0]]

            if len(dup_nodes) > 0:

                dup_node_id = dup_nodes[0][0]
                dup_node = dup_nodes[0][1]

                if dup_node[2] < f:
                    continue
                else:
                    del open_list[dup_node_id]

            graph.nodes[nbr]['parent'] = current_node
            open_list.append((nbr, g, f))

        open_list.sort(key=lambda elem: elem[2])

    return None


# configure osmnx to cache the queries and log what it does (lots of text!)
ox.config(use_cache=True,
          log_console=True)

# use data from file?
load_from_file = False

# positions of some locations
wachsbleiche = (52.2852171, 8.0365810704887)
goal = (52.272235, 8.060066)

# use the geocoding-service instead (WARNING: sometimes this doesn't work)
#wachsbleiche = ox.utils.geocode("Wachsbleiche 27, Osnabrück, Deutschland")

if load_from_file:
    try:
        G = ox.load_graphml('osnabrueck.graphml', folder='data')
    except FileNotFoundError as e:
        raise FileNotFoundError("Graphml file 'osnabrueck.graphml' not found")
else:
    # fetch it from the osm servers
    G = ox.graph_from_point(wachsbleiche, distance=5000, network_type='drive', simplify=False)

# get the closest nodes in the graph for all positions as start and end
wachsbleiche_node = ox.get_nearest_node(G, wachsbleiche)
goal = (52.272235, 8.060066)
goal_node = ox.get_nearest_node(G, goal)

def node_distance(graph, node1, node2):
    """
    Return the distance between the two graph nodes in meters.
    :param graph: networkx.Graph
        The graph to look up the nodes
    :param node1: int
        The node id of the first node
    :param node2: int
        The node id of the second node
    :return: float
        Distance between the two node in meters
    """
    return ox.great_circle_vec(graph.nodes[node1]['x'],
                                 graph.nodes[node1]['y'],
                                 graph.nodes[node2]['x'],
                                 graph.nodes[node1]['y'])

# print the node data for the start
print("start node data:", G.nodes[wachsbleiche_node])

# print the distance between start and goal
print("distance between start and goal:", node_distance(G, wachsbleiche_node, goal_node))


# print the edge data for all edges at the starting node
print("edges around the start:")
for s, e, data in G.edges(wachsbleiche_node, data=True):
    print("\t", data)

# get a route (dijkstra; not the best!!!)
#start_1 = time.time()
#route = nx.shortest_path(G, wachsbleiche_node, goal_node)
#print('Dijkstra time:', time.time() - start_1)
# print(route)

G2 = nx.DiGraph(G)
G3 = nx.MultiDiGraph(G2)

# nicht so konvertieren:
# G2 = nx.Graph(G)
# G3 = nx.MultiGraph(G2)

start = time.time()
route_a = a_star(G, wachsbleiche_node, goal_node)
astar_runtime = time.time() - start

#! Throws not implemented exception for multi graph. So create a graph copy

from functools import partial
h = partial(node_distance, G)

start = time.time()
route_nx = nx.astar_path(G2, wachsbleiche_node, goal_node, heuristic=h, weight='length')
nx_runtime = time.time() - start

print('networkx:')
print('-- runtime:', nx_runtime)
print('-- path length:', route_length(G2, route_nx))
print('own astar:')
print('-- runtime:', astar_runtime)
print('-- path length:', route_length(G, route_a))


if route_a and route_nx:
    # fig, ax = ox.plot_graph_route(G, route, show=False, close=False, axis_off=False, node_color='#66ccff')
    fig, ax = ox.plot_graph_route(G3, route_nx, show=False, close=False, axis_off=False, node_color='#66ccff')
    fig.suptitle("nx astar")
    fig, ax = ox.plot_graph_route(G, route_a, show=False, close=False, axis_off=False, node_color='#66ccff')
    fig.suptitle("astar")
else:
    # plot just the graph
    fig, ax = ox.plot_graph(G, show=False, close=False, axis_off=False, annotate=False)

# plot start and end positions of the route (reverse, because the coordinates are returned in the wrong order for matplotlib!)
ax.scatter(*reversed(wachsbleiche), c='r', s=15, zorder=3)
ax.scatter(*reversed(goal), c='g', s=15, zorder=3)

# print route information
#print("route nodes:")
#for i in range(len(route)-1):
#    print("\t", G.get_edge_data(route[i], route[i+1]))

plt.show()